<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
* [Getting Started](#getting-started)
  * [Dockerfile](#dockerfile)
  * [Pipeline](#pipeline)
* [K8S](#k8s)
* [HPA](#hpa)
* [Contributing](#contributing)
* [License](#license)

<!-- ABOUT THE PROJECT -->
## About The Project

A junior developer of Jobtome has written his first application, available at the following address:
https://github.com/psantori/j2m-sre-test
He is now asking for your help to test, build and push this application to a container registry of
your choice as a docker container using a GitLab CI (hint: use a public project on an ad-hoc free
tier Gitlab account for the sake of the exercise). As a bonus point, considering that the
application will possibly run on Kubernetes, you will add the related manifest files to the
repository.

<!-- GETTING STARTED -->
## Getting Started

### Dockerfile
Following the implementation of a multistage dockerfile used to build and run the golang application

```
FROM golang:1.16-buster AS build
RUN mkdir /app
WORKDIR /app
COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

# =========================================

FROM scratch
WORKDIR /
COPY --from=build /app /app
WORKDIR /app
ENTRYPOINT ["./app"]
```
### Pipeline
On each commit a gitlab CI/CD pipeline will start in order to build tag and push on Gitlab registry the microservice. That's the implementation:

```
  # Use the official docker image.
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  # Default branch leaves tag empty (= latest tag)
  # All other branches are tagged with the escaped branch name (commit ref slug)
  script:
    - |
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag=""
        echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
      else
        tag=":$CI_COMMIT_REF_SLUG"
        echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
      fi
    - docker build --pull -t "$CI_REGISTRY_IMAGE${tag}" .
    - docker push "$CI_REGISTRY_IMAGE${tag}"
  # Run this job in a branch where a Dockerfile exists
  rules:
    - if: $CI_COMMIT_BRANCH
      exists:
        - Dockerfile
```

<!-- k8s -->
## K8S

This command is used to create a deployment on Kubernetes based on the image already built: 

- `kubectl create deployment j2m-sre --image=registry.gitlab.com/mrpr0p3r/j2m-sre-test`

Otherwise it is possible to use the following YAML to create the same resource:
```
cat << EOF > j2m-sre | kubectl apply -f j2m-sre
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: j2m-sre
  name: j2m-sre
spec:
  replicas: 1
  selector:
    matchLabels:
      app: j2m-sre
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: j2m-sre
    spec:
      containers:
      - image: registry.gitlab.com/mrpr0p3r/j2m-sre-test
        name: j2m-sre-test
        resources: {}
status: {}
EOF
```
<!-- hpa -->
## HPA

This kind of autoscaling will create three new replicas if the cpu usage grows up to 75%

- `kubectl autoscale deployment j2m-sre --max=3 --min=1 --cpu-percent=75`

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

