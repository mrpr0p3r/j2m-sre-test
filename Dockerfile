FROM golang:1.16-buster AS build
RUN mkdir /app
WORKDIR /app
COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY *.go ./

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

# =========================================

FROM scratch
WORKDIR /
COPY --from=build /app /app
WORKDIR /app
ENTRYPOINT ["./app"]
